FROM ghcr.io/cargo-lambda/cargo-lambda:latest as builder
WORKDIR /usr/src/app
COPY . .
RUN cargo lambda build --release --arm64

FROM public.ecr.aws/lambda/provided:al2-arm64
WORKDIR /week10

COPY --from=builder /usr/src/app/target/lambda ./ 
COPY --from=builder /usr/src/app/src/pythia-410m-q4_0-ggjt.bin ./ 

ENTRYPOINT ["/week10/bootstrap"]
