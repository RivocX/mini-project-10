// main.rs:
use lambda_http::{run, service_fn, tracing, Body, Error, Request, RequestExt, Response};
use std::convert::Infallible;
use std::io::Write;
use std::path::PathBuf;

/// Function to generate a text response based on a given prompt.
///
/// This function initializes the language model, performs inference,
/// and handles responses to form a complete answer string.
///
/// # Arguments
/// * `prompt` - A string containing the user's input prompt.
///
/// # Returns
/// A result containing the generated string or an error box.
fn generate_response(prompt: String) -> Result<String, Box<dyn std::error::Error>> {
    // Configuration of the model's tokenizer and architecture
    let tokenizer = llm::TokenizerSource::Embedded;
    let architecture = llm::ModelArchitecture::GptNeoX;

    // Define the model file's path based on the environment setup
    let model_file = PathBuf::from("/week10/pythia-410m-q4_0-ggjt.bin");

    // Load the model and initiate a session
    let model = llm::load_dynamic(
        Some(architecture),
        &model_file,
        tokenizer,
        Default::default(),
        llm::load_progress_callback_stdout,
    )?;
    let mut session = model.start_session(Default::default());

    // Container for the generated response text
    let mut response_text = String::new();

    // Execute inference and handle responses incrementally
    let inference_result = session.infer::<Infallible>(
        model.as_ref(),
        &mut rand::thread_rng(),
        &llm::InferenceRequest {
            prompt: (&prompt).into(),
            parameters: &llm::InferenceParameters::default(),
            play_back_previous_tokens: false,
            maximum_token_count: Some(12),
        },
        &mut Default::default(),
        |response| match response {
            llm::InferenceResponse::PromptToken(token) | llm::InferenceResponse::InferredToken(token) => {
                print!("{token}");
                std::io::stdout().flush().unwrap();
                response_text.push_str(&token);
                Ok(llm::InferenceFeedback::Continue)
            }
            _ => Ok(llm::InferenceFeedback::Continue),
        },
    );

    // Determine the final outcome of the inference process
    match inference_result {
        Ok(_) => Ok(response_text),
        Err(e) => Err(Box::new(e)),
    }
}

/// Asynchronous function to handle HTTP requests.
///
/// Extracts the user's query from the request, generates a response
/// using the language model, and constructs an HTTP response.
///
/// # Arguments
/// * `req` - The HTTP request received.
///
/// # Returns
/// A result containing the HTTP response or an error.
async fn handle_request(req: Request) -> Result<Response<Body>, Error> {
    // Extract user query from the HTTP request
    let user_query = req
        .query_string_parameters_ref()
        .and_then(|params| params.first("query"))
        .unwrap_or("Puyang is working on this Cargo project");

    // Generate a response based on the user's query
    let output_message = match generate_response(user_query.to_string()) {
        Ok(result) => result,
        Err(e) => format!("Inference error: {:?}", e),
    };
    println!("Response from model: {:?}", output_message);

    // Build the HTTP response
    let response = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(Body::from(output_message))
        .map_err(Box::new)?;

    Ok(response)
}

/// Main entry point for the server application.
///
/// Initializes the logging subscriber and starts the HTTP server.
#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();
    run(service_fn(handle_request)).await
}
