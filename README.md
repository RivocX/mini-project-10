# Week 10 Mini Project
> Puyang(Rivoc) Xu (px16)

This is a Rust Serverless Transformer Endpoint project.


## Preparation
The installation of `Cargo` is needed in this project.
Create a new cargo project:
```
cargo lambda new serverless_transformer_endpoint
```

Modify dependencies in `Cargo.toml`:
```
[dependencies]
lambda_http = "0.11.1"
tokio = { version = "1", features = ["macros", "rt-multi-thread"] }
tracing = "0.1.27"
log = "0.4.14"
llm = { git = "https://github.com/rustformers/llm" , branch = "main" }
openssl = { version = "0.10.35", features = ["vendored"] }
serde = {version = "1.0", features = ["derive"] }
serde_json = "1.0"
rand = "0.8.5"
```

Download the binary file from [here](https://huggingface.co/rustformers/pythia-ggml/blob/main/pythia-410m-q4_0-ggjt.bin). Put it into `/src` folder. Here I can't push it to this repo because of size limitation. You can download it manually and put it into the folder if you want to test.

Create a IAM role in AWS console, grant it with following permission policies as follows.
![](./media/permissions.png)

Then create a ECR repo named `week10`.


## Test & Deploy
Write `main.rs`. Endpoint is implemented in this file. I chose the pythia-ggml model. In this project, I used `pythia-410m-q4_0-ggjt.bin`. This file can be downloaded from [here](https://huggingface.co/rustformers/pythia-ggml/blob/main/pythia-410m-q4_0-ggjt.bin).

Then you can test locally by running:
```
cargo lambda watch
```

You can run `docker pull` yourself if you encounter some problem pulling.
For instance:
```
docker pull public.ecr.aws/lambda/provided:al2
```

Then build the docker image:
```
sudo docker build -t week10:latest .
```

Then push the image to AWS ECR.
```
# Log in to AWS ECR
aws ecr get-login-password --region <your-region> | docker login --username AWS --password-stdin <your-account-id>.dkr.ecr.<your-region>.amazonaws.com

# Tag the image to match ECR repo
docker tag week10-lambda-image:latest <your-account-id>.dkr.ecr.<your-region>.amazonaws.com/week10:latest

# Push the image to ECR
docker push <your-account-id>.dkr.ecr.<your-region>.amazonaws.com/week10:latest
```

Mine is:
```
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 339712859714.dkr.ecr.us-east-1.amazonaws.com

docker tag week10:latest 339712859714.dkr.ecr.us-east-1.amazonaws.com/week10:latest

docker push 339712859714.dkr.ecr.us-east-1.amazonaws.com/week10:latest
```
![](./media/docker_build.png)

After that, create a AWS Lambda function using existing ECR image. 
Create a new function URL enabling CORS so that we can access it.
![](./media/lambda.jpg)

Now you can test this project by following curl command.
```
curl https://3evercmj6vongprh3sj5hloeum0wyvtx.lambda-url.us-east-1.on.aws/\?text=Puyang%20is%20working%20week10%20project
```
![](./media/curl.png)

## Results & Screenshots

### Local test
![](./media/local.png)

![](./media/lweb.png)

### Lambda function url
![](./media/function.jpg)

### curl result
![](./media/curl.png)

### Lambda monitor
![](./media/monitor.jpg)